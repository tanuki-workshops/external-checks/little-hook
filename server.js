const fastify = require('fastify')({ logger: true })
const path = require('path')

fastify.register(require('fastify-formbody'))
fastify.register(require('fastify-multipart'))

// Serve the static assets
fastify.register(require('fastify-static'), {
  root: path.join(__dirname, ''),
  prefix: '/'
})

fastify.get(`/hello`, async (request, reply) => {
  console.log("hello")
  reply.send({message:"hello"})

  await reply
})


fastify.post(`/ping`, async (request, reply) => {
  console.log("😃", request.body)
  reply.send({message:"pong"})
  await reply
})



const start = async () => {
  try {
    await fastify.listen(3000, "0.0.0.0")
    console.log("server listening on:", fastify.server.address().port)

  } catch (error) {
    fastify.log.error(error)
  }
}
start()
